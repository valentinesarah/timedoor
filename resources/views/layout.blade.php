<html>
  <head>
    <title>Timedoor Challenge - Level 8</title>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/css/font-awesome.min.css') }}">
  </head>

  <body class="bg-lgray">
    <header>
      @extends('header') 
    </header>

    @section('content')
    @show
    @extends('modals') 

    <footer>
      @extends('footer') 
    </footer>
    <script type="text/javascript" src="{{ URL::asset('/assets/js/script.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/assets/js/bootstrap.min.js') }}"></script>
  </body>
</html>