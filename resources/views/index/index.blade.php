@extends('layout')  
@section('content')

<link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/css/tmdrPreset.css') }}">
 <main>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-md-offset-3 bg-white p-30 box">
            <div class="text-center">
              <h1 class="text-green mb-30"><b>Level 8 Challenge</b></h1>
            </div>
            <div class="form-group">
              <label>Name</label>
              <input type="text" class="form-control">
              <p class="small text-danger mt-5">*Your name must be 3 to 16 characters long</p>
            </div>
            <div class="form-group">
              <label>Title</label>
              <input type="text" class="form-control">
            </div>
            <div class="form-group">
              <label>Body</label>
              <textarea rows="5" class="form-control"></textarea>
            </div>
            <div class="form-group">
              <label>Choose image from your computer :</label>
              <div class="input-group">
                <input type="text" class="form-control upload-form" value="No file chosen" readonly>
                <span class="input-group-btn">
                  <span class="btn btn-default btn-file">
                    <i class="fa fa-folder-open"></i>&nbsp;Browse <input type="file" name="image" multiple>
                  </span>
                </span>
              </div>
            </div>
            <div class="form-group">
              <label>Password</label>
              <input type="password" class="form-control">
            </div>
            <div class="text-center mt-30 mb-30">
              <button class="btn btn-primary">Submit</button>
            </div>
            <hr>
            <div class="post">
              <div class="clearfix">
                <div class="pull-left">
                  <h2 class="mb-5 text-green"><b>Here is your title</b></h2>
                </div>
                <div class="pull-right text-right">
                  <p class="text-lgray">05-05-2019<br/><span class="small">14:00</span></p>
                </div>
              </div>
              <h4 class="mb-20">Yutaka Tokunaga <span class="text-id">-</span></h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed laoreet, risus nec suscipit luctus, tortor nibh scelerisque est, nec suscipit justo odio id arcu. Nulla nec sagittis ante, non luctus nulla. Sed imperdiet ullamcorper tortor, ac vulputate mauris. In pulvinar metus eget imperdiet ullamcorper. Vivamus a dolor tempor diam sollicitudin interdum.</p>
              <div class="img-box my-10">
                <img class="img-responsive img-post" src="http://via.placeholder.com/500x500" alt="image">
              </div>
              <form class="form-inline mt-50">
                <div class="form-group mx-sm-3 mb-2">
                  <label for="inputPassword2" class="sr-only">Password</label>
                  <input type="password" class="form-control" id="inputPassword2" placeholder="Password">
                </div>
                <a type="submit" class="btn btn-default mb-2" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil p-3"></i></a>
                <a type="submit" class="btn btn-danger mb-2" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash p-3"></i></a>
              </form>
            </div>
            <div class="post">
              <div class="clearfix">
                <div class="pull-left">
                  <h2 class="mb-5 text-green"><b>Here is your title</b></h2>
                </div>
                <div class="pull-right text-right">
                  <p class="text-lgray">05-05-2019<br/><span class="small">14:00</span></p>
                </div>
              </div>
              <h4 class="mb-20">Yutaka Tokunaga <span class="text-id">1234</span></h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed laoreet, risus nec suscipit luctus, tortor nibh scelerisque est, nec suscipit justo odio id arcu. Nulla nec sagittis ante, non luctus nulla. Sed imperdiet ullamcorper tortor, ac vulputate mauris. In pulvinar metus eget imperdiet ullamcorper. Vivamus a dolor tempor diam sollicitudin interdum.</p>
              <div class="img-box my-15">
                <img class="img-responsive img-post" src="http://via.placeholder.com/1200x500" alt="image">
              </div>
            </div>
            <div class="text-center mt-30">
              <nav>
                <ul class="pagination">
                  <li><a href="#">&laquo;</a></li>
                  <li><a href="#">&lsaquo;</a></li>
                  <li class="active"><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#">5</a></li>
                  <li><a href="#">&rsaquo;</a></li>
                  <li><a href="#">&raquo;</a></li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
<script type="text/javascript" src="{{ URL::asset('/assets/js/validation.js') }}"></script>  
@endsection